// Import required AWS SDK clients and commands for Node.js.
import { PutObjectCommand } from "@aws-sdk/client-s3";
 import { s3 } from "./lib/s3Client.js"; // Helper function that creates Amazon S3 service client module.
//  import { s3 } from "../node_modules/@aws-sdk/client-s3/S3Client"; // Helper function that creates Amazon S3 service client module.
import {path} from "path";
import {fs} from "fs";

// Set the AWS Region.
export function aws_upload(file_path) {
    const REGION = "us-west-1"; //e.g. "us-east-1"
    
    console.log("inside aws", file_path)
    const file = file_path; // Path to and name of object. For example '../myFiles/index.js'.
    const fileStream = fs.createReadStream(file);
    
    // Set the parameters
    const uploadParams = {
      Bucket: "example-images-front-end",
      // Add the required 'Key' parameter using the 'path' module.
      Key: path.basename(file),
      // Add the required 'Body' parameter
      Body: fileStream,
    };
    
    
    // Upload file to specified bucket.
    const run = async () => {
      try {
        const data = await s3.send(new PutObjectCommand(uploadParams));
        console.log("Success", data);
        return data;
      } catch (err) {
        console.log("Error", err);
      }
    };
    run();
}
