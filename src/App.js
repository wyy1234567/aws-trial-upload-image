import React, { Component } from 'react';
import './App.css';
// import axios from 'axios';
import { aws_upload } from './aws_upload_file.js';
class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      show_images: false,
      img1: "",
      img2: "",
      selected_file: ""
    }
  }

  handleButton = () => {
    this.setState({
      show_images: true,
      img1: "https://example-images-front-end.s3-us-west-1.amazonaws.com/T1T555TL0-U01UP64PJSD-5831d731d236-512.jpg",
      img2: "https://example-images-front-end.s3-us-west-1.amazonaws.com/T1T555TL0-U40TQU2KB-b3e7a99323a3-512.jpg"
    })
  }

  renderImage = () => {
    return (
      <div className="image-container">
        <img src={this.state.img1}></img>
        <img src={this.state.img2}></img>
      </div>
    )
  }


  handleUpload = () => {
    aws_upload(this.state.selected_file)
  }

  onFileChange = (event) => {
    this.setState({
      selected_file: event.target.files[0]
    })
    console.log("file upload in appjs,", this.state.selected_file)
  }
  render() {

    return (
      <div className="App">
        <input type="file" onChange={this.onFileChange} />
        <button onClick={this.handleUpload}>Upload!</button>
        <button onClick={this.handleButton}>Get!</button>
        {this.state.show_images
        ? this.renderImage()
        : ""}

      </div>
    );
  }

}

export default App;
